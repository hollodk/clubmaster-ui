<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends AbstractController
{
    #[Route('/', name: 'app_default')]
    public function index(): Response
    {
        return $this->redirectToRoute('app_default_admin');
    }

    #[Route('/admin')]
    public function admin(): Response
    {
        return $this->render('default/admin.html.twig', [
            'page_title' => 'Admin',
            'body_style' => 'sidebar-mini',
            'sidebar' => true,
        ]);
    }

    #[Route('/public')]
    public function public(): Response
    {
        return $this->render('default/public.html.twig', [
            'page_title' => 'Public',
            'body_style' => 'layout-top-nav',
            'sidebar' => false,
        ]);
    }

    #[Route('/user')]
    public function user(): Response
    {
        return $this->render('default/user.html.twig', [
            'page_title' => 'User',
            'body_style' => 'sidebar-mini',
            'sidebar' => true,
        ]);
    }

    #[Route('/users')]
    public function users(): Response
    {
        return $this->render('default/users.html.twig', [
            'page_title' => 'Users',
            'body_style' => 'sidebar-mini',
            'sidebar' => true,
        ]);
    }
}
