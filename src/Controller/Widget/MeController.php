<?php

namespace App\Controller\Widget;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/widget/me')]
class MeController extends AbstractController
{
    #[Route('/index')]
    public function index(Request $request): Response
    {
        $client = new \GuzzleHttp\Client();

        $options = [
            'headers' => [
                'User-Agent' => 'testing/1.0',
                'X-AUTH-TOKEN' => $request->query->get('token'),
            ],
        ];

        $json = null;

        try {
            $res = $client->request('GET', 'https://app.optikpartner.dk/api/v1/me', $options);

            $json = $res->getBody()->getContents();

        } catch (\GuzzleHttp\Exception\ClientException $e) {
            dd($e->getResponse()->getBody()->getContents());

        } catch (\GuzzleHttp\Exception\ServerException $e) {
            dd($e->getResponse()->getBody()->getContents());

        }

        return $this->render('widget/me/index.html.twig', [
            'json' => $json,
        ]);
    }
}
