<?php

namespace App\Controller\Widget;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

#[Route('/widget/appointment')]
class AppointmentController extends AbstractController
{
    #[Route('/index')]
    public function index(): Response
    {
        return $this->render('widget/appointment/index.html.twig', [
        ]);
    }
}
