ClubMaster UI
=============

Requirements
------------

This project depends on the following packages:

- symfony-cli
- composer
- npm


Getting started
---------------

1.
First clone repository

2.
First install node packages for graphical dependencies

$ npm install

3.
Then install symfony framework for routings

$ composer install

4.
everything is now setup, start serve the public directory, easiest way is to use symfony as

$ symfony server:start

and a webserver will now be fired up at **http://localhost:8000** or whatever the program reports to your console


Happy coding
------------

:)
