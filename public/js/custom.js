$(document).ready(function() {
    console.log('starting widget processer');

    $('*[data-widget-href]').each(function() {
        var e = $(this);

        var url = e.data('widget-href')+'?token='+token;
        console.log('loading content from '+url);

        $.get({
            url: url,
        })
        .done(function(res) {
            e.html(res);
        })
        ;
    })
    ;
});
